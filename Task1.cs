﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BezoaCSharpTask
{
    class Task1
    {
        public static void GetAge()
        {  
            Console.Write("Enter Your Fullname : ");
            string name = Console.ReadLine();
            string dob = "";
            string days = "+";
            DateTime birthDayDatetime=DateTime.Today;
            do
            {
                Console.Write("Enter your Birthday (Month/Day/Year): ");
                dob = Console.ReadLine();
                try{
                    birthDayDatetime = Convert.ToDateTime(dob);
                    TimeSpan timeSpan = birthDayDatetime.Subtract(DateTime.Today);
                    days = Convert.ToString(timeSpan).Split(".")[0];
                    if (days[0] != '-')
                        Console.WriteLine("Your age is in correct");
                }
                catch (System.FormatException){
                    Console.WriteLine("Error: Age is in Wrong Format");
                }
            } while (days[0] != '-');

            double years = Math.Round(Math.Abs( Convert.ToInt32(days.Split('.')[0]) / 365.0)); 
            double days_ = (Convert.ToInt32(days.Split('.')[0]) % 365.0);
            Console.WriteLine($"Hi {name}, you are {years} years  and  {Math.Abs(days_)} days old");
            
        }

    }
}
